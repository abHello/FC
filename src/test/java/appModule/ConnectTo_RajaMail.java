package appModule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import pageObject.MailBoxRaja;
import utility.Data;

public class ConnectTo_RajaMail {
	public static void login(WebDriver driver, String email, String password) throws Exception{
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get(Data.url_mailRAJA);
			MailBoxRaja.FirstEmail_fld(driver).sendKeys(email);
			MailBoxRaja.Next_btn(driver).click();
			Thread.sleep(2000);
			MailBoxRaja.Password_fld(driver).sendKeys(password);
			MailBoxRaja.Submit_btn(driver).click();
			MailBoxRaja.BeConnected_Modal(driver).click();
			
		}catch(Exception e) {
			throw(e);
		}
		
		
	}

}


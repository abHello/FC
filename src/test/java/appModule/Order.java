package appModule;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import pageObject.Delivery;
import pageObject.Ogone;
import pageObject.Payement;
import utility.Data;

public class Order {

	
	public static void DoOrder(WebDriver driver, int i) throws Exception {
		  try {
				
			  PayementLanding.Payement(driver);
			  Delivery.ValidateShipping_btn(driver).click();
			  Thread.sleep(1000);
			  List<WebElement> lst=Payement.PayementMethods(driver).findElements(By.tagName("li"));
			  lst.get(i).click();
			  Thread.sleep(1000);
			  Actions actions = new Actions(driver);
			  actions.moveToElement(Payement.CGV_CheckBox(driver)).click().build().perform();
			  Payement.Validate_btn(driver).click();
			  
			  if(i==0) {
				  Ogone.CardNumber(driver).sendKeys(Data.CardNumber);
				  
				  Select slct=new Select(Ogone.SelectMonth(driver));
				  slct.selectByIndex(5);
				  
				  slct=new Select(Ogone.SelectYear(driver));
				  slct.selectByIndex(5);
				  
				  Ogone.CVC(driver).sendKeys(Data.CVC);
				  Ogone.Confirm_btn(driver).click();
			  }
		  
		} catch (Exception e) {
			System.out.println("erreur : La commande ne passe pas.");
			throw(e);
		}
	  }

}



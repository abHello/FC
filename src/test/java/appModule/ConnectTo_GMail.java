package appModule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import pageObject.MailBox;
import utility.Data;

public class ConnectTo_GMail {
	public static void login(WebDriver driver, String email, String password) throws Exception{
		try {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driver.get(Data.url_Gmail);
			MailBox.Email_fld(driver).sendKeys(email);
			MailBox.Next_btn(driver).click();
			Thread.sleep(1000);
			MailBox.Password_fld(driver).sendKeys(password);
			Thread.sleep(1000);
			MailBox.Submit_btn(driver).click();
			if(MailBox.Gmail_Icone(driver).isDisplayed()) {
				MailBox.Gmail_Icone(driver).click();
			}
			
		}catch(Exception e) {
			throw(e);
		}
		
		
	}

}


package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Univer {
public static WebElement element=null;
	
	public static WebElement Univers(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='product__item-grid']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Univers n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Navigation(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//ul[@class='level0']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Univers n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Filter(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='product__filters']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Filter n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Product(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='product__grid']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Produit n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element; 
		}

}

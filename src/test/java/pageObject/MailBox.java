package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MailBox {
public static WebElement element=null;
	
	public static WebElement Email_Table(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//table[@class='F cf zt']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Email_Table n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Email_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='identifierId']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Email_fld n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Next_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@id='identifierNext']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Next_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Password_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@type='password']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Password_fld n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Submit_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//span[@class='RveJvd snByac']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Submit_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Gmail_Icone(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='vmZ0T']/a[2]"));
		
		}catch(Exception e) {
			System.out.println("L'élément Gmail_Icone n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

}

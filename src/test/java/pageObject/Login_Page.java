 package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login_Page {
public static WebElement element=null;
	
	public static WebElement Login_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//a[@class='btns red arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Login_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Email(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ShopLoginForm_Login']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Login_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Password(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='ShopLoginForm_Password']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Login_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Submit(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns red arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Login_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement AlertFakeLogin(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='alert alert-error']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AlertFakeLogin n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Disabled_submit(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns red arrowr disabled']"));
		
		}catch(Exception e) {
			System.out.println("L'élément boutton grisé n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement ForgottenPassword(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//div[@class='form-group text-center']/a[1]"));
		
		}catch(Exception e) {
			System.out.println("L'élément ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement CreateAccout(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//div[@class='form-group text-center']/a[2]"));
		
		}catch(Exception e) {
			System.out.println("L'élément ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Email_ForgottenPassword(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//input[@id='ForgotPasswordStep1Email_Login']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Email_ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Submit_ForgottenPassword(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//button[@class='btns arrowr red']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Submit_ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement ObligatoryEmail_ForgottenPassword(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//small[@data-fv-for='ForgotPasswordStep1Email_Login']"));
		
		}catch(Exception e) {
			System.out.println("L'élément ObligatoryEmail_ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement EmailNotFound_ForgottenPassword(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//div[@class='alert alert-danger']"));
		
		}catch(Exception e) {
			System.out.println("L'élément ObligatoryEmail_ForgottenPassword n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Register(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//div[@class='sign--up text-center']/span/a"));
		
		}catch(Exception e) {
			System.out.println("L'élément Register n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement FirstCommande_Alert(WebDriver driver) throws Exception {
		
		try {
			element=driver.findElement(By.xpath("//div[@class='k-popup-close']"));
		
		}catch(Exception e) {
			throw(e);
		}
		return element;
		}

}

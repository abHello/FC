package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegistrationForm {
	
public static WebElement element=null;
	
	public static WebElement FormTitle(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//h1[@class='block__title blue no-pd']"));
		
		}catch(Exception e) {
			System.out.println("L'élément FormTitle n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

	
	public static WebElement Email_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Email']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Email_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Password_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Password']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Password_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Show_Password_box(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//ins[@class='iCheck-helper']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Show_Password_box n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement CompanyName_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_CompanyName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément CompanyName_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Siret_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_SIRET']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Siret_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement CheckSiret_box(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='customer__line']/div[3]/div[3]/div"));
		
		}catch(Exception e) {
			System.out.println("L'élément CheckSiret_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement SiretQuestion_select(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//select[@id='RegistrationForm_SiretQuestion']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SiretQuestion_select n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Title_Select(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//select[@id='RegistrationForm_Title']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Title_Select n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement FirstName_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_FirstName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément FirstName_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement LastName_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_LastName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément LastName_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement PhoneHome_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_PhoneHome']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PhoneHome_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Mobile_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Mobile']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Mobile_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}

	
	
	public static WebElement Service_select(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//select[@id='RegistrationForm_Service']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Service_select n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Job_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Function']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Job_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Adresse1_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Address1']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Adresse1_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Adresse2_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Address2']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Adresse2_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Adresse3_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Address3']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Adresse3_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement PostalCode_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_PostalCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PostalCode_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement City_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_City']"));
		
		}catch(Exception e) {
			System.out.println("L'élément City_fld n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Country_select(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//select[@id='RegistrationForm_CountryCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Country_select n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Newsletter_box(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_Newsletter']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Newsletter_box n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement NewsletterBusiness_box(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='RegistrationForm_NewsletterBusiness']"));
		
		}catch(Exception e) {
			System.out.println("L'élément NewsletterBusiness_box n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Submit_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@id='registerSubmit']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Submit_btn n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;		
	}
	
	
	public static WebElement Particular(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='form-inline m-b-10']/div[2]"));
		
		}catch(Exception e) {
			System.out.println("L'élément Particular n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;		
	}
	
	
	public static WebElement Company_box(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='form-inline m-b-10']/div"));
		
		}catch(Exception e) {
			System.out.println("L'élément Particular n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;		
	}
	
		


}

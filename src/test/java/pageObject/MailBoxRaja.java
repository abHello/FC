package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MailBoxRaja {
public static WebElement element=null;
	
	public static WebElement Email_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='userNameInput']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Email_fld n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement FirstEmail_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@name='loginfmt']"));
		
		}catch(Exception e) {
			System.out.println("L'élément FirstEmail_fld n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Password_fld(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='passwordInput']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Password_fld n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Submit_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//span[@id='submitButton']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Submit_btn n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement BeConnected_Modal(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='inline-block']"));
		
		}catch(Exception e) {
			System.out.println("L'élément BeConnected_Modal n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Next_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='idSIButton9']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Next_btn n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Subject(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='_lvv_M _lvv_S']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Subject n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Time(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"_ariaId_29\"]/div[2]/div[4]/span"));
		
		}catch(Exception e) {
			System.out.println("L'élément Time n'a pas été trouvé.");
			throw(e);
		}
		return element;
		}

}

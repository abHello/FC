package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Basket {
public static WebElement element=null;
	
	public static WebElement RefProd(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@placeholder='Réf. du produit']"));
		
		}catch(Exception e) {
			System.out.println("L'élément RefProd n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement TableProduct(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"section_main\"]/div[2]/div[2]/div/div/form/div[2]/div/div/div[1]/span/div/div/div/table"));
		
		}catch(Exception e) {
			System.out.println("L'élément TableProduct n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement AddToBasket_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns red arrowr fright']"));
		
		}catch(Exception e) {
			System.out.println("L'élément AddToBasket_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement SeeBasket_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='fright']/a"));
		
		}catch(Exception e) {
			System.out.println("L'élément SeeBasket_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Table_Ref(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//table[@class='m-b-20 checkout-panier-table']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Table_Ref n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement TablePrice_Recap(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//table[@class='box-recap__table-order']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Table_Price n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement AddQuantity(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"cart-container\"]/div[2]/div[1]/form/table/tbody/tr/td[3]/span/a[1]"));
		
		}catch(Exception e) {
			System.out.println("L'élément AddQuantity n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement PromotionCode(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='promotionCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PromotionCode n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement SubmitCode(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns bdrred arrowr icon']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SubmitCode n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement CodeAction_Msg(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='actions-message']"));
		
		}catch(Exception e) {
			System.out.println("L'élément SubmitCode n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement DoOrder_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@class='btns red arrowr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément DoOrder_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement IgnoreCart_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//button[@id='ignoreCart']"));
		
		}catch(Exception e) {
			System.out.println("L'élément IgnoreCart_btn n'a pas été trouvé.");
			throw(e);
		}
		return element;
	}

}

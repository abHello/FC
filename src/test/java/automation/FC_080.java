package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_080 {
	WebDriver driver=null;
	
	  @Test
	  public void C_080() throws Exception {
		  try {
			Assert.assertTrue(Home.StaticPage_sitemap(driver).getText().contains(Data.StaticPage_sitemap));

		} catch (Exception e) {
			System.out.println("erreur sitemap : fichier StaticPage n'existe pas.");
			throw(e);
		}
	  }


	  @BeforeMethod
	  public void beforeTest() {
	 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
	 			driver=new ChromeDriver();
	 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	 			driver.get(Data.url_SiteMap);
	 					}
	  @AfterMethod
	  public void afterTest() {
	 			driver.quit();
	 					}

}

package automation;

import org.testng.annotations.Test;

import appModule.Login;
import pageObject.Home;
import pageObject.My_Account;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_039 {
	WebDriver driver=null;
	
  @Test
  public void C_039() throws Exception {
	  try {
		Home.MyAccount_btn(driver).click();  
		Login.login(driver);
		Home.MyAccount_Connected(driver).click();
		Home.See_Profil_btn(driver).click();
		Assert.assertTrue(My_Account.Account_Heading(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("Problème dans la création du compte e-shop FR.");
		throw(e);
	}
  }

  @BeforeMethod
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterMethod
  public void afterTest() {
			  	driver.quit();
				}

}

package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_037 {
	WebDriver driver=null;
	Select country=null;
	String Countries[]=null;
	

	@Test
	  public void C_037() throws Exception {
			  try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Register(driver).click();
				
				Countries=new String[3];
				Countries[1]="FR";
				Countries[2]="MC";
				
				country=new Select(RegistrationForm.Country_select(driver));
				System.out.println(country.getOptions().get(1).getAttribute("value"));
				
				for(int i=1;i<country.getOptions().size(); i++) {
					Assert.assertTrue(country.getOptions().get(i).getAttribute("value").equals(Countries[i]));
				}
			} catch (Exception e) {
				System.out.println("Les pays France, Monaco n'existent pas dans le select.");
				throw(e);
			}
		  }

	  @BeforeMethod
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterMethod
	  public void afterTest() {
				  	driver.quit();
					}


}

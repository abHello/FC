package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_051 {
	WebDriver driver=null; 

 @Test
 public void C_051() throws Exception { 
		  try {
			  BasketLanding.Basket(driver); 
			  Basket.DoOrder_btn(driver).click();
			  Login.login(driver);
			  Assert.assertTrue(Basket.TablePrice_Recap(driver).isDisplayed());
			  
		} catch (Exception e) {
			System.out.println("erreur : recap n'est pas affiché dans la page livraison..");
			throw(e);
		}	  
	  }

 @BeforeMethod
 public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
					}
 @AfterMethod
 public void afterTest() {
			driver.quit();
					}

}

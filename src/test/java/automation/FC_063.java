package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_063 { 
	WebDriver driver=null;
	Set<String> tab_handles=null; 
	

  @Test
  public void C_063() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  Delivery.ValidateShipping_btn(driver).click();
		  Payement.CGV_Link(driver).click();
		  
		  tab_handles = driver.getWindowHandles();
		  driver.switchTo().window(tab_handles.toArray()[1].toString());
		  
		  Assert.assertTrue(Payement.CGV_Page(driver).isDisplayed());
		  
	  } catch (Exception e) {
			System.out.println("erreur : La page statique CGV n'est pas accessible.");
			throw(e);
		}
  
  }

  @BeforeMethod
	  public void beforeTest() {
	 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
	 			driver=new ChromeDriver();
	 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	 			driver.get(Data.url);
	 					}
  @AfterMethod
	  public void afterTest() {
	 			driver.quit();
	 					}

}

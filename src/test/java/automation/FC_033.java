package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.ObligatoryInformation;
import pageObject.RegistrationForm;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_033 {
	WebDriver driver=null;
	
	  @Test
	  public void C_033() throws Exception {
			  try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Register(driver).click();
				RegistrationForm.PostalCode_fld(driver).sendKeys(Data.PostalCode_Incorrect);
				RegistrationForm.Submit_btn(driver).click();
				Assert.assertTrue(ObligatoryInformation.PostalCode_Incorrect(driver).isDisplayed());
			} catch (Exception e) {
				System.out.println("Code postal erroné: message d'erreur n'est pas affiché.");
				throw(e);
			}
		  }

	  @BeforeMethod
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterMethod
	  public void afterTest() {
				  	driver.quit();
					}

}

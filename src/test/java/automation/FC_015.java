package automation;

import org.testng.annotations.Test;

import appModule.Login;

import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_015 {
	WebDriver driver=null;
	
  @Test
  public void C_015() throws Exception {
	  try {
		   
		Home.MyAccount_btn(driver).click();
		try { 
			if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
				Login_Page.FirstCommande_Alert(driver).click();
			}}catch(Exception e) {
			}
		Login.login(driver);
		Assert.assertTrue(Home.MyAccount_Connected(driver).getText().contains(Data.Email));
	} catch (Exception e) {
		System.out.println("Le login à un compte ne fonctionne pas."); 
		throw(e);
	}
	  
  }


  @BeforeMethod
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterMethod
  public void afterTest() {
	  	driver.quit();
  		}

}

package automation;

import org.testng.annotations.Test;

import appModule.PDT_Landing;
import pageObject.PDT;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_012 {
	WebDriver driver=null;
	
  @Test
  public void C_012() throws Exception {
	  try {
		PDT_Landing.PDT(driver);
		Assert.assertNotNull(PDT.Titre(driver).getText());
		
	} catch (Exception e) {
		System.out.println("Le titre n'est pas affiché dans PDT.");
		throw(e);
	}
  }

  @BeforeMethod
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterMethod
  public void afterTest() {
	  	driver.quit();
  		}

}

package automation;

import org.testng.annotations.Test;

import appModule.Registration;
import pageObject.Home;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_038 {
	WebDriver driver=null;
	
  @Test
  public void C_038() throws Exception {
	  try {
		Registration.Account_Creation(driver);
		Assert.assertTrue(Home.MyAccount_Connected(driver).getText().contains(Registration.email.substring(0, 10)));
	} catch (Exception e) {
		System.out.println("Problème dans la création du compte e-shop FR.");
		throw(e);
	}
  }

  @BeforeMethod
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterMethod
  public void afterTest() {
			  	driver.quit();
				}

}

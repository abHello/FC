package automation;

import org.testng.annotations.Test;

import appModule.PDT_Landing;
import pageObject.BaliseSEO;
import pageObject.Home;
import utility.Data;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_083 {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_081_1() throws Exception {
	  try {  
	  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()<165);
	  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()>10);
	  
	} catch (Exception e) {
		System.out.println("erreur balise SEO : taille 'meta-description' incorrecte dans la home.");
		throw(e);
	}
}
  
  
  @Test
  public void C_081_2() throws Exception {
	  try {
		  lst=Home.BigMenu_lst(driver).findElements(By.tagName("li"));
		  lst.get(0).click();
		  lst=Home.Univers_menu(driver).findElements(By.tagName("li"));
		  lst.get(1).click();

		  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()<165);
		  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()>10);
	  
	} catch (Exception e) {
		System.out.println("erreur balise SEO : taille 'meta-description' incorrecte dans la famille produit.");
		throw(e);
	}
}
  
  
  @Test
  public void C_081_3() throws Exception {
	  try {
		  PDT_Landing.PDT(driver);

		  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()<165);
		  Assert.assertTrue(BaliseSEO.meta_Description(driver).getAttribute("content").length()>10);
	  
	} catch (Exception e) {
		System.out.println("erreur balise SEO : taille 'meta-description' incorrecte dans PDT.");
		throw(e);
	}
} 

  @BeforeMethod
  public void beforeTest()  {
		System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(Data.url);
 					}
  @AfterMethod
  public void afterTest() {
 			driver.close();
 					} 


}

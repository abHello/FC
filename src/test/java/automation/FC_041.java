package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import pageObject.Basket;
import utility.Data;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_041 {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_041() throws Exception {
	  try {
		  BasketLanding.Basket(driver);
		  lst=Basket.Table_Ref(driver).findElements(By.tagName("td"));
		  Assert.assertTrue(lst.get(1).getText().contains(Data.Commande_Ref));
	} catch (Exception e) {
		System.out.println("La liste des références n'est pas présente dans la page panier.");
		throw(e);
	}
	    
	  
  }

  @BeforeMethod
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterMethod
  public void afterTest() {
			  	driver.quit();
				}


}

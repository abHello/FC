package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Prices;
import pageObject.Basket;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_046 {
	WebDriver driver=null;
	Double TotalHT, ShippingCost=null; 
	
	  @Test
	  public void C_046() throws Exception {
		  try {
			  BasketLanding.Basket(driver); 
			  ShippingCost=Prices.ShippingCost(driver);
			  TotalHT=Prices.Total_HT(driver);
			  Basket.PromotionCode(driver).sendKeys(Data.CodeAction_New_FR);
			  Basket.SubmitCode(driver).click();
			  Thread.sleep(1000);
			  Assert.assertEquals(Prices.Total_HT_freeShipping(driver), TotalHT-ShippingCost);
			  
		} catch (Exception e) {
			System.out.println("erreur : Prix total n'est pas réduit avec le code action 'bienvenue'.");
			throw(e);
		}	  
	  }

	  @BeforeMethod
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterMethod
	  public void afterTest() {
				  	driver.quit();
					}

}

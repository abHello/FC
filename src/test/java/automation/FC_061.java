package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;

import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_061 {
	WebDriver driver=null; 
	String str=null;
	
  @Test
  public void C_061() throws Exception {
	  try {
		  
		  PayementLanding.Payement(driver);
		  Thread.sleep(1000);
		  Delivery.ValidateShipping_btn(driver).click();
		  Payement.Bloc_address(driver).click();
		  str=Delivery.AdressName(driver).getAttribute("value");
		  
		  Delivery.AdressName(driver).clear();
		  Delivery.AdressName(driver).sendKeys(str+"a");
		  
		  Payement.Save_btn(driver).click();
		  Delivery.ReturnToDelivery_btn(driver).click();
		  
		  Payement.Bloc_address(driver).click();
		  
		  Assert.assertTrue(Delivery.AdressName(driver).getAttribute("value").equals(str+"a"));
		  
		  Delivery.AdressName(driver).clear();
		  Delivery.AdressName(driver).sendKeys(str);
		  Payement.Save_btn(driver).click();
		  Delivery.ReturnToDelivery_btn(driver).click();
	  }catch(Exception e) {
		  System.out.println("erreur : adresse de facturation ne peut être modifiée.");
		  throw(e);
	  }
  }

  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}

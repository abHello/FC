package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import pageObject.RegistrationForm;
import utility.Data;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_029 {
	WebDriver driver=null;
	
  @Test
  public void C_029() throws Exception {
		  try {
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Register(driver).click();
			RegistrationForm.Password_fld(driver).sendKeys(Data.Password);
			RegistrationForm.Show_Password_box(driver).click();
			Assert.assertTrue(RegistrationForm.Password_fld(driver).getAttribute("value").equals(Data.Password));

		} catch (Exception e) {
			System.out.println("Affiché les caractères du password ne donctionne pas.");
			throw(e);
		}
	  }

  @BeforeMethod
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterMethod
  public void afterTest() {
			  	driver.quit();
				}

}

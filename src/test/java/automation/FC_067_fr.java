package automation;

import org.testng.annotations.Test;

import appModule.Order;
import pageObject.Payement;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_067_fr {
	WebDriver driver=null;
	
  @Test
  public void C_067_fr() throws Exception {
	  try {
		  Order.DoOrder(driver, 5);
		  
		  Assert.assertTrue(Payement.OrderConfirmation(driver).isDisplayed());
	  
	} catch (Exception e) {
		System.out.println("erreur : La confirmation de commande ne s'affiche pas quand 'Virement' est choisi.");
		throw(e);
	}
  }

  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}

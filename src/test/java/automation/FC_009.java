package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Univer;
import utility.Data;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_009 {
	WebDriver driver=null;
	List<WebElement> MenuNumber=null;
	
  @Test
  public void C_009() {
		try {
			MenuNumber=Home.BigMenu_lst(driver).findElements(By.tagName("li"));
			MenuNumber.get(0).click();
			MenuNumber=Home.Univers_menu(driver).findElements(By.tagName("li"));
			MenuNumber.get(1).click();
			MenuNumber=Univer.Navigation(driver).findElements(By.tagName("li"));
			Assert.assertFalse(MenuNumber.size()==0);
		} catch (Exception e) {
			System.out.println("La navigation de la sous famille ne s'affichent pas correctement.");
			e.printStackTrace();
		}
		
  }
 
  @BeforeMethod
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterMethod
  public void afterTest() {
	  	driver.quit();
  		}

}

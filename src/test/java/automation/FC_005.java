package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_005 {
	WebDriver driver=null;
	
  @Test
  public void C_004() throws Exception {
	  try {
		Assert.assertTrue(Home.MyAccount_btn(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("Mon compte de la home n'est pas affiché");
		throw(e);
	}
  }
  

  @BeforeMethod
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterMethod
  public void afterTest() {
	  	driver.quit();
  		}

}

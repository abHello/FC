package automation;

import org.testng.annotations.Test;

import appModule.ConnectTo_GMail;
import pageObject.Home;
import pageObject.Login_Page;
import pageObject.MailBox;
import utility.CompareHours;
import utility.Data;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_022 {
	WebDriver driver=null;
	List<WebElement> MailBoxSize=null;
	int hour1,minute1, hour2,minute2=0;
	Calendar cal=null;
	
  @Test
  public void C_022() throws Exception{
		try {
			
			
			
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Login_btn(driver).click();
			Login_Page.ForgottenPassword(driver).click();
			Login_Page.Email_ForgottenPassword(driver).sendKeys(Data.Email);
			Login_Page.Submit_ForgottenPassword(driver).click();
			
			cal = Calendar.getInstance(); 
	        hour1 = cal.get(Calendar.HOUR_OF_DAY);
	        minute1 = cal.get(Calendar.MINUTE);

			ConnectTo_GMail.login(driver,Data.Email,Data.Password);
			MailBoxSize=MailBox.Email_Table(driver).findElements(By.tagName("td"));
			Assert.assertTrue(MailBoxSize.get(5).getText().equals("Service client Raja"));
			
	        hour2 = cal.get(Calendar.HOUR_OF_DAY);
	        minute2 = cal.get(Calendar.MINUTE);
	        CompareHours.Compare(hour1+":"+minute1, MailBoxSize.get(7).getText());
			
		}catch(Exception e) {
			System.out.println("Mot de passe oublié : un email n'est pas envoyé à la boite mail.");
			throw(e);
	
		}
  }
 

	@BeforeMethod
	public void beforeTest() {
		System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
			}
	@AfterMethod
	public void afterTest() {
		  	driver.quit();
			}

}

package automation;

import org.testng.annotations.Test;

import appModule.PayementLanding;
import pageObject.Delivery;
import pageObject.Payement;
import utility.Data;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_065_fr {
	WebDriver driver=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_065_fr() throws Exception {
	  try {
		  PayementLanding.Payement(driver);
		  Delivery.ValidateShipping_btn(driver).click();
		  lst=Payement.PayementMethods(driver).findElements(By.tagName("li"));

		  Assert.assertTrue(lst.get(0).getText().contains("CB en ligne"));
		  Assert.assertTrue(lst.get(5).getText().contains("Virement"));
		  Assert.assertTrue(lst.get(6).getText().contains("Chèque"));
		  Assert.assertTrue(lst.get(7).getText().contains("Paypal"));
	} catch (Exception e) {
		System.out.println("erreur : Méthode de paiement manquante.");
		throw(e);
	}

  }
  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}

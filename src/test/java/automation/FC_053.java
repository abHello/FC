package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import pageObject.Delivery;
import utility.Data;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_053 {
	WebDriver driver=null;
	String str=null;
	List<WebElement> lst=null;
	
  @Test
  public void C_053() throws Exception {
	  try {
		  Thread.sleep(1000);
		  BasketLanding.Basket(driver);
		  Basket.DoOrder_btn(driver).click();
		  Login.login(driver);

		  Delivery.SeeAdresses_btn(driver).click();
		  Thread.sleep(1000);
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));

		  str=lst.get(1).getText();
		  lst.get(1).click(); 
		  Delivery.Validate_btn(driver).click();

		  Delivery.Contact_select(driver).click();
		  Delivery.Validate_btn(driver).click();
		  
		  Assert.assertEquals(str.substring(0,10), Delivery.Bloc_address(driver).getText().substring(0,10));
		  
	} catch (Exception e) {
		System.out.println("erreur : adresse de livraison n'est pas sélectionnée.");
		throw(e);
	} 

  }

  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}

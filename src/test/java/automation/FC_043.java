package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Prices;

import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_043 {
	WebDriver driver=null;

	
	  @Test
	  public void C_043() throws Exception { 
		  try {
			  BasketLanding.Basket(driver);
			  Assert.assertEquals(Prices.Total_HT(driver), Prices.TotalProduct_HT(driver)+Prices.ShippingCost(driver)+Prices.RajaGarantie(driver));
			  Assert.assertEquals(Prices.Total_TTC(driver), Prices.TVA(driver)+Prices.Total_HT(driver));
		} catch (Exception e) {
			System.out.println("erreur : montant total n'est pas égal à la somme des autres frais.");
			throw(e);
		}
		    
		  
	  }

	  @BeforeMethod
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); 
					driver.get(Data.url);
					}
	  @AfterMethod
	  public void afterTest() {
				  	driver.quit();
					}

}

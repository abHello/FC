package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import pageObject.Basket;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_047 {
	WebDriver driver=null;  

	  @Test
	  public void C_047() throws Exception {
		  try {
			  BasketLanding.Basket(driver); 
			  Basket.PromotionCode(driver).sendKeys(Data.CodeAction_Fake);
			  Basket.SubmitCode(driver).click();
			  Assert.assertTrue(Basket.CodeAction_Msg(driver).isDisplayed());
			  
		} catch (Exception e) {
			System.out.println("erreur : message d'erreur n'est pas affiché quand code action est erroné.");
			throw(e);
		}	  
	  }

	  @BeforeMethod
	  public void beforeTest() {
				System.setProperty(Data.DriverProperty, Data.ChromeDriver);
					driver=new ChromeDriver();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.get(Data.url);
					}
	  @AfterMethod
	  public void afterTest() {
				  	driver.quit();
					}

}

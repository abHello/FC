package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_021 {
	WebDriver driver=null;
	
	@Test
	  public void C_021() throws Exception{
			try {
				Home.MyAccount_btn(driver).click();
				try { 
					if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
						Login_Page.FirstCommande_Alert(driver).click();
					}}catch(Exception e) {
					}
				Login_Page.Login_btn(driver).click();
				Login_Page.ForgottenPassword(driver).click();
				Login_Page.Email_ForgottenPassword(driver).sendKeys(Data.Fake_Email);
				Login_Page.Submit_ForgottenPassword(driver).click();
				Assert.assertTrue(Login_Page.EmailNotFound_ForgottenPassword(driver).isDisplayed());
				
			}catch(Exception e) {
				System.out.println("Information obligatoire n'est pas affichée.");
				throw(e);
			}
	}

	@BeforeMethod
	public void beforeTest() {
		System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
			}
	@AfterMethod
	public void afterTest() {
		  	driver.quit();
			}

}

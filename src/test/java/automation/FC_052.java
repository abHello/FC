package automation;

import org.testng.annotations.Test;

import appModule.AddDeliveryAddress;
import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import pageObject.Delivery;
import utility.AddressesNumber;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_052 {
	WebDriver driver=null; 
	int nbAdd=0;
	
 @Test
 public void C_052() throws Exception { 
		  try {
			  Thread.sleep(1000);
			  BasketLanding.Basket(driver); 
			  Basket.DoOrder_btn(driver).click();
			  Login.login(driver);
			  
			  nbAdd=AddressesNumber.Number(Delivery.SeeAdresses_btn(driver).getText());
			  Thread.sleep(1000);
			  Delivery.SeeAdresses_btn(driver).click();
			  Delivery.AddAdresse_btn(driver).click();
			  AddDeliveryAddress.AddAddress(driver);
			  
			  Assert.assertEquals(AddressesNumber.Number(Delivery.SeeAdresses_btn(driver).getText()), nbAdd+1);
			  
		} catch (Exception e) {
			System.out.println("erreur : adresse de livraison n'est pas créée.");
			throw(e);
		}	  
	  }

 @BeforeMethod
 public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.get(Data.url);
					}
 @AfterMethod
 public void afterTest() {
			driver.quit();
					}

}

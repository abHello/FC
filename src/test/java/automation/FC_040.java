package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import pageObject.Basket;
import utility.Data;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_040 {
	WebDriver driver=null;

	
  @Test
  public void C_040() throws Exception {
	  try {
		  BasketLanding.Basket(driver);
		  Assert.assertTrue(Basket.Table_Ref(driver).isDisplayed());
	} catch (Exception e) {
		System.out.println("La page Panier n'est pas accessible.");
		throw(e);
	}	  
  }

  @BeforeMethod
  public void beforeTest() {
			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
				driver=new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.get(Data.url);
				}
  @AfterMethod
  public void afterTest() {
			  	driver.quit();
				}

}

package automation;

import org.testng.annotations.Test;

import appModule.BasketLanding;
import appModule.Login;
import pageObject.Basket;
import pageObject.Delivery;
import utility.Data;
import utility.RandomString;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_054 {
	WebDriver driver=null;
	String contact=null;
	List<WebElement> lst=null;
	int i=0;
	
  @Test
  public void C_054() throws Exception {
	  try {
		  
		  BasketLanding.Basket(driver);
		  Basket.DoOrder_btn(driver).click();
		  Login.login(driver);

		  Delivery.SeeAdresses_btn(driver).click();
		  
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));
		  lst.get(2).click();
		  
		  Delivery.Validate_btn(driver).click();
		  lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));
		  
		  if(lst.size()==1) {
				Delivery.AddContact_btn(driver).click();
				Delivery.AddContact_link(driver).click();
				Delivery.Contact_new_FirstName(driver).sendKeys(RandomString.NameString());
				Delivery.Contact_new_LastName(driver).sendKeys(Data.LastName);
				Delivery.Contact_new_Email(driver).sendKeys(Data.Email);
				Delivery.Contact_new_Phone(driver).sendKeys(Data.MobileFR);
				Delivery.Save_btn(driver).submit();
				Delivery.ReturnToDelivery_btn(driver).click();
				
				/*Delivery.SeeAdresses_btn(driver).click();
				Delivery.Validate_btn(driver).click();
				lst=Delivery.Adresses_group(driver).findElements(By.tagName("div"));*/
				
		  }

		  if(Delivery.Contact(driver).getText().equals(lst.get(0).getText())) {
				  i=2;
		  }else {
				  i=0;
		  }
			  
			  lst.get(i).click();
			  contact=lst.get(i).getText();
			  Delivery.Validate_btn(driver).click();
  
		  Assert.assertTrue(contact.contains(Delivery.Contact(driver).getText().substring(0, Delivery.Contact(driver).getText().length()-2)));
	} catch (Exception e) {
		System.out.println("erreur : contact lié à l'adresse de livraison n'est pas sélectionné."); 
		throw(e);
	}

  }


  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
  @AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}

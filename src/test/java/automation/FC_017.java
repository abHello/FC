package automation;

import org.testng.annotations.Test;

import pageObject.Home;
import pageObject.Login_Page;
import utility.Data;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class FC_017 {
	WebDriver driver=null;
	
  @Test
  public void C_017() throws Exception{
		try {
			Home.MyAccount_btn(driver).click();
			try { 
				if(Login_Page.FirstCommande_Alert(driver).isDisplayed()) {
					Login_Page.FirstCommande_Alert(driver).click();
				}}catch(Exception e) {
				}
			Login_Page.Login_btn(driver).click();
			Login_Page.Email(driver).sendKeys(Data.Fake_Email);
			Login_Page.Password(driver).sendKeys(Data.Password);
			Login_Page.Submit(driver).click();
			Assert.assertTrue(Login_Page.AlertFakeLogin(driver).isDisplayed());
		}catch(Exception e) {
			System.out.println("Faux login à un compte ne fonctionne pas : email invalide, mot de passe valide."); 
			throw(e);
		}
}

  @BeforeMethod
  public void beforeTest() {
	System.setProperty(Data.DriverProperty, Data.ChromeDriver);
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Data.url);
  		}
  @AfterMethod
  public void afterTest() {
	  	driver.quit();
  		}

}

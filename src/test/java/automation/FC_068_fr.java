package automation;

import org.testng.annotations.Test;

import appModule.ConnectTo_RajaMail;
import appModule.Order;
import pageObject.MailBoxRaja;
import utility.CompareHours;
import utility.Data;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

import org.testng.annotations.BeforeMethod;

public class FC_068_fr {
	WebDriver driver=null;
	int hour1,minute1=0;  
	Calendar cal=null;
	
  @Test
  public void C_068_fr() throws Exception {
	  try {
		  
		  	cal = Calendar.getInstance();
	        hour1 = cal.get(Calendar.HOUR_OF_DAY);
	        minute1 = cal.get(Calendar.MINUTE);

		  	Order.DoOrder(driver, 5);

	        ConnectTo_RajaMail.login(driver, Data.Email_Raja, Data.Password_Raja);

	        CompareHours.Compare(hour1+":"+minute1, MailBoxRaja.Time(driver).getText());
	        
	        Assert.assertTrue(MailBoxRaja.Subject(driver).getText().contains("Confirmation de commande"));
		  
	  } catch (Exception e) {
			System.out.println("erreur : email de confirmation de commande n'est pas envoyé au CRC.");  
			throw(e);
		}
	  }

  @BeforeMethod
  public void beforeTest() {
 			System.setProperty(Data.DriverProperty, Data.ChromeDriver);
 			driver=new ChromeDriver();
 			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 			driver.get(Data.url);
 					}
@AfterMethod
  public void afterTest() {
 			driver.quit();
 					}

}
